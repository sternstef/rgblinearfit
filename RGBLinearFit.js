/* PURPOSE
   This script for PixInsight performs a linear fit of all channels from a given RGB image
   and should be applied directly after stacking in the linear state.

   LICENSE

   Copyright (C) 2022 Stefan Schmid.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

 */

#feature-id    Utilities > RGBLinearFit

#feature-info  This script performs a linear fit of a given RGB image. \
               Should be applied directly after stacking in the linear state.


#include <pjsr/Sizer.jsh>
#include <pjsr/TextAlign.jsh>

#define VERSION "1.01"
#define TITLE   "RGBLinearFit"


// define a global variable containing scripts parameters
var RGBLinearFitParameters = {
   refChannel: 'R',
   keepWorkChannels: false,
   targetView: undefined,

   // stores the current parameter values into the script instance
   save: function() {
      Parameters.set("refChannel", RGBLinearFitParameters.refChannel);
      Parameters.set("keepWorkChannels", RGBLinearFitParameters.keepWorkChannels);
   },

   // loads the script instance parameters
   load: function() {
      if (Parameters.has("refChannel"))
         RGBLinearFitParameters.refChannel = Parameters.getString("refChannel")
      if (Parameters.has("keepWorkChannels"))
         RGBLinearFitParameters.keepWorkChannels = Parameters.getBoolean("keepWorkChannels")
   }

}


/*
 * This function applies a RGBWorkingSpace with identical luminance coefficients (=1)
 * for all RGB channels to a given view
 */
function applyRGBWorkingSpace(view) {

   // Instantiate process
   var P = new RGBWorkingSpace;
   P.channels = [ // Y, x, y
     [1.000000, 0.648431, 0.330856],
     [1.000000, 0.321152, 0.597871],
     [1.000000, 0.155886, 0.066044]
   ];
   P.gamma = 2.20;
   P.sRGBGamma = true;
   P.applyGlobalRGBWS = false;

   // Perform the transformation
   P.executeOn(view);
}

function applyGlobalRGBWorkingSpace(view) {

   // Instantiate process
   var P = new RGBWorkingSpace;
   P.applyGlobalRGBWS = true;

   // Perform the transformation
   P.executeOn(view);
}

function applyChannelExtraction(view) {

   var CHANNEL_R = view.id + '_R';
   var CHANNEL_G = view.id + '_G';
   var CHANNEL_B = view.id + '_B';

   // Instantiate process
   var P = new ChannelExtraction;
   P.colorSpace = ChannelExtraction.prototype.RGB;
   P.channels = [ // enabled, id
      [true, CHANNEL_R],
      [true, CHANNEL_G],
      [true, CHANNEL_B]
   ];
   P.sampleFormat = ChannelExtraction.prototype.SameAsSource;
   P.inheritAstrometricSolution = true;

   // Perform the transformation
   P.executeOn(view);

}

function applyLinearFit(view, refChannel) {

   const channels = ["R", "G", "B"];

   var viewChannel = new Object();

   viewChannel['R'] = new View(view.id + '_R');
   viewChannel['G'] = new View(view.id + '_G');
   viewChannel['B'] = new View(view.id + '_B');

   channels.forEach((ch) => {
    if(ch != refChannel) {
     var P = new LinearFit;
     P.referenceViewId = viewChannel[refChannel].id;
     P.rejectLow = 0.000000;
     P.rejectHigh = 0.920000;
     P.executeOn(viewChannel[ch]);
    }

   });

}


function applyChannelCombination(view) {

   var CHANNEL_R = view.id + '_R';
   var CHANNEL_G = view.id + '_G';
   var CHANNEL_B = view.id + '_B';

   // Instantiate process
   var P = new ChannelCombination;
   P.colorSpace = ChannelCombination.prototype.RGB;
   P.channels = [ // enabled, id
      [true, CHANNEL_R],
      [true, CHANNEL_G],
      [true, CHANNEL_B]
   ];
   P.inheritAstrometricSolution = true;

   // Perform the transformation (global)
   P.executeGlobal();

}  

function closeWorkChannels(view) {

   var viewChannel = new Object();

   viewChannel['R'] = new View(view.id + '_R');
   viewChannel['G'] = new View(view.id + '_G');
   viewChannel['B'] = new View(view.id + '_B');

   Console.writeln("Closing RGB channel views...");

   viewChannel['R'].window.forceClose();
   viewChannel['G'].window.forceClose();
   viewChannel['B'].window.forceClose();

   Console.writeln("done");

}


function RGBLinearFitDialog() {
   this.__base__ = Dialog;
   this.__base__();

   // global window settings
   this.windowTitle = TITLE + " Script";
   this.adjustToContents();
   this.userResizable = false;
   this.minWidth = 540;
   var labelMinWidth = Math.round(this.font.width("Keep temporary RGB images:") + 2.0 * this.font.width('X'));

   // script title
   this.title = new TextBox(this);
   this.title.text = "<b>" + TITLE + " v" + VERSION + "</b> &mdash; This script performs a linear fit of a given RGB image." +
                     "<br><br>Author: Stefan Schmid"
   this.title.readOnly = true;
   this.title.minHeight = 80;
   this.title.maxHeight = 80;

   // view list label
   this.viewListLabel = new Label(this);
   this.viewListLabel.text = "Target image:";
   this.viewListLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   // view list
   this.viewList = new ViewList(this);
   this.viewList.getAll();

   // set active image window as default target view
   var window = ImageWindow.activeWindow;
   if ( !window.isNull ) {
      this.viewList.currentView = window.currentView;
   }

   RGBLinearFitParameters.targetView = this.viewList.currentView;
   this.viewList.onViewSelected = function (view) {
      RGBLinearFitParameters.targetView = view;
   }

   // refChannel label
   this.refChannelLabel = new Label(this);
   with (this.refChannelLabel) {
      text = "Reference Channel:";
      minWidth = labelMinWidth;
      textAlignment = TextAlign_Left|TextAlign_VertCenter;
   }

   // refChannel combobox
   this.refChannelComboBox = new ComboBox(this);
   with ( this.refChannelComboBox )
   {

      toolTip = "Reference channel (R/G/B) for linear fit";
      this.refChannelComboBox.maxWidth = 50;
   
      addItem('R');
      addItem('G');
      addItem('B');

      RGBLinearFitParameters.refChannel = itemText(0);

      onItemSelected = function( index )
      {
         RGBLinearFitParameters.refChannel = itemText(index)
      }
   }

   // refChannel combobox layout
   this.refChannelComboBoxSizer = new HorizontalSizer;
   with (this.refChannelComboBoxSizer) {
      add(this.refChannelLabel)
      add(this.refChannelComboBox)
      addStretch();
   }

   // keepWorkingChannels label
   this.keepWorkChannelsLabel = new Label(this);
   with (this.keepWorkChannelsLabel) {
      text = "Keep temporary RGB images:";
      minWidth = labelMinWidth;
      textAlignment = TextAlign_Left|TextAlign_VertCenter;
   }

   // keepWorkChannels checkbox
   this.keepWorkChannelsCheckBox = new CheckBox(this);
   with (this.keepWorkChannelsCheckBox){
      toolTip = "Keep temporary RGB channel windows after linear fit";
      onCheck = function( checked )
      {
         RGBLinearFitParameters.keepWorkChannels = checked;
      }
   }

   // keepWorkChannels checkbox layout
   this.keepWorkChannelsSizer = new HorizontalSizer;
   with (this.keepWorkChannelsSizer) {
      add(this.keepWorkChannelsLabel)
      add(this.keepWorkChannelsCheckBox)
      addStretch();
   }

   // instance button
   this.newInstanceButton = new ToolButton( this );
   with (this.newInstanceButton){
      icon = this.scaledResource( ":/process-interface/new-instance.png" );
      setScaledFixedSize( 24, 24 );
      toolTip = "New Instance";
      onMousePress = () => {
         // stores the parameters
         RGBLinearFitParameters.save();
         // create the script instance
         this.newInstance();
      };
   }

   // execution button
   this.execButton = new PushButton(this);
   with (this.execButton){
      text = "Ok";
      icon = new Bitmap( ":/icons/ok.png" );
      toolTip = "Perform linear fit";
      onClick = () => {
         this.ok();
      }
   }

   // cancel button
   this.cancelButton = new PushButton(this);
   with (this.cancelButton){
      text = "Cancel";
      icon = new Bitmap( ":/icons/cancel.png" );
      toolTip = "Cancel script execution";
      onClick = () => {
         this.cancel();
      }
   }

   // button layout
   this.ButtonSizer = new HorizontalSizer;
   with (this.ButtonSizer){
      add(this.newInstanceButton);
      addStretch();
      add(this.execButton);
      addSpacing(8);
      add(this.cancelButton);
   }


   // dialog
   this.sizer = new VerticalSizer;
   with (this.sizer) {
      add(this.title);
      addSpacing(8);
      add(this.viewListLabel);
      addSpacing(8);
      add(this.viewList);
      addSpacing(8);
      add(this.refChannelComboBoxSizer);
      addSpacing(8);
      add(this.keepWorkChannelsSizer);
      addSpacing(8);
      add(this.ButtonSizer);
      addStretch();
      margin = 8;
   }

}

RGBLinearFitDialog.prototype = new Dialog;


function main() {

   // show console for execution
   Console.show();

   // perform the script on the target view
   if (Parameters.isViewTarget) {
      // load parameters
      RGBLinearFitParameters.load();
      applyRGBWorkingSpace(Parameters.targetView);
      applyChannelExtraction(Parameters.targetView);
      applyLinearFit(Parameters.targetView, RGBLinearFitParameters.refChannel);
      applyChannelCombination(Parameters.targetView);
      // remove working channel views
      if (!RGBLinearFitParameters.keepWorkChannels) {
         closeWorkChannels(Parameters.targetView);
      }
      // rename global target view
      ImageWindow.activeWindow.currentView.id = Parameters.targetView.id + "_LF";
      applyGlobalRGBWorkingSpace(ImageWindow.activeWindow.currentView);
      // hide console after execution
      Console.hide();
      return;
      
   }

   // is script started from an instance in global context?
   if (Parameters.isGlobalTarget) {
      // load the parameters from the instance
      RGBLinearFitParameters.load();
   }

   // hide console for dialog
   Console.hide();

   let dialog = new RGBLinearFitDialog;
   let execReturn = dialog.execute();

   if (execReturn == 1) {
      // check if a valid target view has been selected
      if (RGBLinearFitParameters.targetView && RGBLinearFitParameters.targetView.id) {
         // show console for execution
         Console.show();
         // perform linear fit
         applyRGBWorkingSpace(RGBLinearFitParameters.targetView);
         applyChannelExtraction(RGBLinearFitParameters.targetView);
         applyLinearFit(RGBLinearFitParameters.targetView, RGBLinearFitParameters.refChannel);
         applyChannelCombination(RGBLinearFitParameters.targetView);
         // remove working channel views
         if (!RGBLinearFitParameters.keepWorkChannels) {
            closeWorkChannels(RGBLinearFitParameters.targetView);
         }
         // rename global target view
         ImageWindow.activeWindow.currentView.id = RGBLinearFitParameters.targetView.id + "_LF";
         applyGlobalRGBWorkingSpace(ImageWindow.activeWindow.currentView);
         // hide console after execution
         Console.hide();

      } else {
         Console.warningln("No target view is specified");
      }
   } else {
         Console.warningln("Script execution has been terminated by user");
   }

}

main();
