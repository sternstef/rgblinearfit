# RGBLinearFit

## Purpose
This script for PixInsight performs a linear fit of all channels from a given RGB image.

## Usage
The script should be applied directly after the stacking procedure.

## License

Copyright (C) 2022 Stefan Schmid.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.


